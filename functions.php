<?php
/**
 * Constants and definitions
**/
	define('THEME_PATH', get_template_directory());
	define('THEME_DIR', get_bloginfo('template_directory'));
	define('CSS_DIR', THEME_DIR . '/css/');
	define('JS_DIR', THEME_DIR . '/js/');
	define('IMAGE_DIR', THEME_DIR . '/images/');
	define('INC', THEME_PATH . '/includes/');
	define('DISALLOW_FILE_EDIT', true);

/**
 * Theme resources and requirements
**/
	$assets = array(
		'functions' => array('general', 'post-types', 'taxonomies'),
		'libraries' => array(
			'Theme', 'Theme/Post',
			'Utility', 'Utility/String'
		)
	);

	foreach ($assets as $directory_name => $file_name) {
		$directory = sprintf('%s/%s', INC, $directory_name);

		if (file_exists($directory)) {
			require_once sprintf('%s/%s.php', $directory_name, $file_name);
		}
	}