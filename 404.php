<?php get_header(); ?>

	<main id="site-main" class="page-not-found" role="main">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="page">
			<h2 class="page-title">Page not found</h2>
		</article>
	<?php endwhile; endif; ?>
	</main>

<?php get_footer(); ?>