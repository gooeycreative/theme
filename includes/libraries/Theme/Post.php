<?php
/**
 *
**/
	namespace Theme;

	class Post {
		public static function getPostData($id, $key) {
			$_post = get_post($id);

			if ($_post) {
				$_post = (array) $_post;

				if (array_key_exists($key, $_post)) {
					return $_post[$key];
				}
			}

			return false;
		}
	}