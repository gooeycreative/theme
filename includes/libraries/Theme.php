<?php
/**
 * Theme related methods
**/
	final class Theme {
		public static function getStylesheet($name) {
			Theme::assertFileExistence(THEME_PATH . '/css/' . $name . '.' . $extension);
			return CSS_DIR . $name . '.css';
		}

		public static function getScript($name) {
			Theme::assertFileExistence(THEME_PATH . '/js/' . $name . '.' . $extension);
			return CSS_DIR . $name . '.js';
		}

		public static function getImage($name, $extension = 'png') {
			Theme::assertFileExistence(THEME_PATH . '/images/' . $name . '.' . $extension);
			return IMAGE_DIR . $name . '.' . $extension;
		}

		private static function assertFileExistence($path) {
			if (!file_exists($path)) {
				$file_directory = explode('/', $path);
				$file_name = array_pop($file_directory);
				trigger_error(sprintf('Unable to locate "%s" in "%s"', $file_name, implode('/', $file_directory)));
				return false;
			}
			
			return true;
		}
	}