<?php
/**
 * A theme utilities library
**/
	class Utility {
		public static function debug($var, $function = 'print_r', $end = false) {
			echo '<pre class="debug">';
			$function($var);
			echo '</pre>';

			if (true === $end) {
				exit;
			}
		}
	}