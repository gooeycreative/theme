<?php
/**
 *
**/
	namespace Utility;

	class String {
		public static function obfuscate($string = '') {
			$obfuscation = $string;

			if (strlen($string) > 0) {
				for ($i = 0, $obfuscation = ''; $i < strlen($string); $i++) {
					$obfuscation .= sprintf('&#%s;', ord($string[$i]));
				}
			}

			return $obfuscation;
		}
	}