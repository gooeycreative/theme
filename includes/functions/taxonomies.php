<?php
/**
 * Register any necessary taxonomies to be used throughout the build
 */
	function register_taxonomies() {
		$taxonomies = array(
			/*'taxonony_name' => array(
				'post_types' => array('post'),
				'args' => array(
					'labels' => array(
						'name' => 'Taxonomy Names',
						'singular_name' => 'Taxonomy Name',
						'menu_name' => 'Taxonomy Names',
						'all_items' => 'All Taxonomy Names',
						'parent_item' => 'Parent Taxonomy Name',
						'parent_item_colon' => 'Parent Taxonomy Name:',
						'new_item_name' => 'New Taxonomy Name Name',
						'add_new_item' => 'Add New Taxonomy Name',
						'edit_item' => 'Edit Taxonomy Name',
						'update_item' => 'Update Taxonomy Name',
						'separate_items_with_commas' => 'Separate taxonomy names with commas',
						'search_items' => 'Search Taxonomy Names',
						'add_or_remove_items' => 'Add or remove taxonomy names',
						'choose_from_most_used' => 'Choose from the most used taxonomy names',
						'not_found' => 'Not Found'
					),
					'hierarchical' => true,
					'public' => true,
					'show_ui' => true,
					'show_admin_column' => true,
					'show_in_nav_menus' => true,
					'show_tagcloud' => true,
					'rewrite' => array('slug' => 'blogs-category')
				)
			)*/
		);

		if (!empty($taxonomies)) {
			foreach ($taxonomies as $name => $args) {
				register_taxonomy($name, $args['post_types'], $args['args']);
			}
		}
	}