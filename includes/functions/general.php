<?php
/**
 * Action and filter hooks
**/
	add_action('init', 'register_post_types');
	add_action('init', 'register_taxonomies');
	add_action('wp_enqueue_scripts', 'load_resources');
	add_filter('wpseo_metabox_prio', '__return_low');

/**
 * Theme support
**/
	add_theme_support('html5');
	add_theme_support('post-thumbnails');

/**
 * Functions
**/
	function load_resources() {
		if (is_admin() || in_array($GLOBALS['pagenow'], array('wp-register.php', 'wp-login.php'))) {
			return;
		}

		/* stylesheets */
		wp_enqueue_style('theme-styles', Theme::getStylesheet('main'));

		/* required libraries */
		if (wp_script_is('jquery', 'registered')) {
			wp_deregister_script('jquery');
		}

		wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js', array(), '1.12.1', false);

		/* plugins */
		wp_enqueue_script('modernizr', Theme::getScript('plugins/modernizr'), array(), false, false);

		/* theme */
		wp_enqueue_script('theme-scripts', Theme::getScript('dist/main'), array('jquery'), false, true);
	}

	function __return_low() {
		return 'low';
	}