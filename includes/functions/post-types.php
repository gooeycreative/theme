<?php
/**
 * Register any post types necessary for the theme to function.
**/
	function register_custom_post_types() {
		$post_types = array(
			/*'post_type_name' => array(
				'labels' => array(
					'name' => _x('Post Type Names', 'post_type_name'),
					'singular_name' => _x('Post Type Name', 'post_type_name'),
					'add_new' => _x('Add New', 'post_type_name'),
					'add_new_item' => _x('Add New Post Type Name', 'post_type_name'),
					'edit_item' => _x('Edit Post Type Name', 'post_type_name'),
					'new_item' => _x('New Post Type Name', 'post_type_name'),
					'view_item' => _x('View Post Type Name', 'post_type_name'),
					'search_items' => _x('Search Post Type Names', 'post_type_name'),
					'not_found' => _x('No Post Type Names found', 'post_type_name'),
					'not_found_in_trash' => _x('No Post Type Names found in Trash', 'post_type_name'),
					'parent_item_colon' => _x('Parent Post Type Name:', 'post_type_name'),
					'menu_name' => _x('Post Type Names', 'post_type_name')
				),
				'hierarchical' => true,
				'description' => '',
				'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
				'taxonomies' => array(),
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'menu_icon' => 'dashicons-list-view',
				'menu_position' => 5,
				'show_in_nav_menus' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => false,
				'has_archive' => true,
				'query_var' => true,
				'can_export' => true,
				'rewrite' => array('slug' => 'post-type-slug'),
				'capability_type' => 'post'
			)*/
		);

		if (!empty($post_types)) {
			foreach ($post_types as $name => $args) {
				register_post_type($name, $args);
			}
		}
	}