<?php get_header(); ?>

	<main id="site-main" class="page" role="main">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="page <?php post_class(); ?>" id="page-<?php echo $post->ID; ?>">
			<h2 class="page-title"><?php the_title(); ?></h2>
			<?php the_content(); ?>
		</article>
	<?php endwhile; endif; ?>
	</main>

<?php get_footer(); ?>