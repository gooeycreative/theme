var gulp = require('gulp');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');
var watch = require('gulp-watch');
var argv = require('yargs').argv;

const notifier = require('node-notifier');

/**
 * Task definitions
 */
gulp.task('scss', function() {
	var path = './scss/**/*.scss';

	if (argv.compress) {
		gulp.src(path)
			.pipe(sass({ outputStyle: 'compressed' })).on('error', swallowError)
			.pipe(rename('main.css')).on('error', swallowError)
			.pipe(gulp.dest('./css/'));
	} else {
		gulp.src(path)
			.pipe(sourcemaps.init())
			.pipe(sass()).on('error', swallowError)
			.pipe(rename('main.css')).on('error', swallowError)
			.pipe(sourcemaps.write('maps')).on('error', swallowError)
			.pipe(gulp.dest('./css/'));
	}
});

gulp.task('babel', function() {
	if (argv.compress) {
		gulp.src('./js/src/**/*.js')
			.pipe(babel({ presets: ['es2015'] })).on('error', swallowError)
			.pipe(uglify()).on('error', swallowError)
			.pipe(rename('main.js')).on('error', swallowError)
			.pipe(gulp.dest('./js/'));
	} else {
		gulp.src('./js/src/**/*.js')
			.pipe(babel({ presets: ['es2015'] })).on('error', swallowError)
			.pipe(rename('main.js')).on('error', swallowError)
			.pipe(gulp.dest('./js/'));
	}
});

gulp.task('watch', function() {
	if (!argv.nowatch) {
		gulp.watch('./scss/**/*.scss', ['scss']);
		gulp.watch('./js/src/**/*.js', ['babel']);
	}
});

gulp.task('default', ['scss', 'babel', 'watch']);

/**
 * Error handling
 */
function swallowError(error) {
	console.log(error.toString());

	try {
		notifier.notify({
			title: 'ERROR:',
			message: error.formatted.toString().trim()
		});
	} catch (e) {
		notifier.notify({
			title: 'ERROR:',
			message: error.toString()
		});
	}

	this.emit('end');
}