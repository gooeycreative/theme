<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php wp_head(); ?>

		<script>
			window.uri = '<?php echo home_url('/'); ?>';
			window.debug = '<?php echo WP_DEBUG ? 'true' : 'false'; ?>';
		</script>
	</head>