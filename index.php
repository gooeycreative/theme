<?php get_header(); ?>

	<main id="site-main" class="index" role="main">
		<div class="posts-wrapper">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="post <?php post_class(); ?>" id="post-<?php echo $post->ID; ?>">
				<h2 class="post-title"><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</article>
		<?php endwhile; endif; ?>

			<footer class="posts-footer">
				<?php paginate_links(); ?>
			</footer>
		</div>
	</main>

<?php get_footer(); ?>