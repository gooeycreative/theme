# This is the theme...
...that you should be using for your WordPress sites. This basic documentation will cover general usage, customising 
specific elements, and reporting issues that need to be resolved in the future, as well as temporary fixes.

## Getting started
Getting started with the theme is fairly simple. All you need to do is clone the master branch from the repository to 
wherever you need it on your system:

```
git clone -b master https://bitbucket.org/gooeycreative/theme.git
```

As long as your system is authenticated in Git properly, then you'll be able to clone the above without issues. 
**Remember that you'll need to remove the `.git` directory that gets created when you clone it as well.** If you leave 
this in your theme, then it's going to link elsewhere in your repository for the theme, and cause problems.

Then you'll need to open up the command line, navigate to your theme, and run `npm install`. Once that's finished, then 
you're good to set Gulp going to compile your styles and scripts.

## Compilation (transpilation?)
Super easy. In your command line tool, whilst still in your theme's directory, just run `gulp`. That's it. And all of 
your styles and scripts are compiled down. One thing you might notice is that it only runs once, and then doesn't really 
do much else. The reason for this is because the Gulpfile is slowly being developed further for additional 
functionality in the future, so we've included additional arguments that allow you to do things like watch the build 
for changes, or compress the files for production.

So to keep the task running while you're making changes, then all you need to do is run:
`gulp watch`

And it's going to watch your files until you cancel it.

![alt text](https://m.popkey.co/06d9f7/eLa3e.gif)

### New functionality
Whilst I can appreciate that it might be cool if you have a new idea and you want to see it implemented right off the 
bat, that's not always going to be the case. So if you have some thoughts for additional code that you'd like to see in 
the base theme, want a library integrating, or anything similar, then [create a new issue](https://bitbucket.org/gooeycreative/theme/issues/new) 
on the repository so I'm aware of it.

When creating an issue, use as much detail as possible in the description and include reference links if you've got any, 
as well as update the criteria to the following:

**Assignee:** Matt Royce  
**Kind:** Enhancement